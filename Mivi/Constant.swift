//
//  Constant.swift
//  Mivi
//
//  Created by Brajesh on 5/8/18.
//  Copyright © 2018 Muthukumar. All rights reserved.
//

import Foundation


let detailViewControllerIdentifier = "detailViewControllerIdentifier"
let detailTableViewCell = "DetailTableViewCell"
let detailTableViewCellIdentifier = "detailTableViewCellIdentifier"
let otherIdentifier = "otherIdentifier"
