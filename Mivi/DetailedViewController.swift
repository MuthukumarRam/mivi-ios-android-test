//
//  DetailedViewController.swift
//  Mivi
//
//  Created by Brajesh on 5/8/18.
//  Copyright © 2018 Muthukumar. All rights reserved.
//

import UIKit

enum sectionTitle: String {
    case Subscription
    case Product
    case Other
}

class DetailedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var title_screen: String?
    var subscription: SubscriptionDetails?
    var product: ProductDetails?
    var subscriptionArray: [Any] = []
    var subscriptionLeftValue: [String] = []
    var productLeftValue: [String] = []
    var otherLeftValue: [String] = []


    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        subscriptionArray.append(subscription?.dataBalance ?? 0)
        subscriptionArray.append(subscription?.expiryDate ?? "")
        subscriptionLeftValue.append("Data Balance")
        subscriptionLeftValue.append("Expiry Date")
        productLeftValue.append("Plan Name")
        productLeftValue.append("Price")
        otherLeftValue.append("Auto Renewal")
        otherLeftValue.append("Primary Subscription")
        self.tableView.tableFooterView = UIView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.hidesBackButton = true
        self.title = title_screen
        let rightBarButton = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.plain, target: self, action: #selector(logout(_:)))
        rightBarButton.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = rightBarButton


    }
    
    @objc func logout(_: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    // Get the instance of the viewcontroller
    class func newViewcotroller() -> DetailedViewController {
        let viewcontroller = DetailedViewController.instantiateWithIdentifier(identifier: "DetailedViewController") as! DetailedViewController
        return viewcontroller
    }
    
    // TableView delegate and datasource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount: Int = 0
        if section == 0 {
            rowCount =  subscriptionArray.count
        }else  if section == 1 {
            rowCount =  productLeftValue.count
        }else  if section == 2 {
            rowCount =  productLeftValue.count
        }
        return rowCount
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var sectionTitle: sectionTitle?
        if section == 0 {
            sectionTitle = .Subscription
        }else if section == 1 {
            sectionTitle = .Product
        }else {
            sectionTitle = .Other
        }
        return sectionTitle.map { $0.rawValue }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailTableViewCellIdentifier", for: indexPath) as! DetailTableViewCell
                cell.label.text = subscriptionLeftValue[indexPath.row]
                cell.labelValue.text = "\(subscriptionArray[indexPath.row])"
               return cell
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailTableViewCellIdentifier", for: indexPath) as! DetailTableViewCell

             cell.label.text = productLeftValue[indexPath.row]
            if indexPath.row == 0 {
                cell.labelValue.text = product!.product_Name
            }else if indexPath.row == 1 {
                 cell.labelValue.text = "\(product!.price)"
            }
               return cell
        }else if indexPath.section == 2 {
            let cell_other = tableView.dequeueReusableCell(withIdentifier: otherIdentifier) as! OtherTableViewCell
            cell_other.leftLabel.text = otherLeftValue[indexPath.row]
            if indexPath.row == 0 {
                cell_other.switch_button.setOn((subscription?.auto_renewal)!, animated: false)
            }else if indexPath.row == 1 {
                cell_other.switch_button.setOn((subscription?.primary_subscription)!, animated: false)
            }
            return cell_other
            
        }
     return UITableViewCell()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
