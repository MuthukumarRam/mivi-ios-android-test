//
//  ViewController+Extension.swift
//  Mivi
//
//  Created by Brajesh on 5/8/18.
//  Copyright © 2018 Muthukumar. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController {
    
func showTextAlertMessage(title: String, message: String) {
    let alertcontroller = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
    let okAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
    alertcontroller.addAction(okAction)
    present(alertcontroller, animated: true, completion: nil)
}
    
    static func instantiateWithIdentifier(identifier: String) -> UIViewController {
        //If we use multiple storyboard add one more parameter for storyboard filename.
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: identifier)
        return viewController
    }
}

