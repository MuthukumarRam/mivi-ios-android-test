//
//  ViewController.swift
//  Mivi
//
//  Created by Muthukumar on 08/05/18.
//  Copyright © 2018 Muthukumar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var user: UserInfo?
    var subscription: SubscriptionDetails?
    var product: ProductDetails?
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.readLocalJsonFile()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // Parse the local json file
     func readLocalJsonFile() {
        if let urlPath = Bundle.main.url(forResource: "collection", withExtension: "json") {
            do {
                let jsonData = try Data(contentsOf: urlPath, options: .mappedIfSafe)
                if let jsonDict = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: AnyObject] {
                    if let personArray = jsonDict["included"] as? [[String: AnyObject]] {
                        self.getSubcriptionDetails(subscriptions: personArray[1])
                        self.getProductDetails(product: personArray[2])

                    }
                    if let personDictt = jsonDict["data"] as? [String: AnyObject] {
                        guard let personId = personDictt["id"] as? String else {
                            return
                        }
                        guard let personType = personDictt["type"] as? String else {
                            return
                        }
                        guard let attributes = personDictt["attributes"] as? [String:Any] else {
                            return
                        }
                        guard let contactNumber = attributes["contact-number"] as? String else {
                            return
                        }
                        guard let emailAddress = attributes["email-address"] as? String else {
                            return
                        }
                        guard let dateOfBirth = attributes["date-of-birth"] as? String else {
                            return
                        }
                        guard let firstName = attributes["first-name"] as? String else {
                            return
                        }
                        guard let lastName = attributes["last-name"] as? String else {
                            return
                        }
                        guard let title_value = attributes["title"] as? String else {
                            return
                        }
                        self.user =  UserInfo(id: personId, type: personType, contantnumber: contactNumber, dateofbirth: dateOfBirth, emailaddress: emailAddress, firstname: firstName, lastname: lastName, title: title_value)
                    }
                }
            }
                
            catch let jsonError {
                print(jsonError)
            }
        }
    }
    
    // Get the subcription details
    func getSubcriptionDetails(subscriptions: [String:Any]) {
        var attributes : [String:Any] =  subscriptions["attributes"] as! [String : Any]
        guard let suscription_id = subscriptions["id"] as? String else {
            return
        }
        guard let type = subscriptions["type"] as? String else {
            return
        }
        guard let dataBalance = attributes["included-data-balance"] as? Int else {
            return
        }
        guard let expireDate = attributes["expiry-date"] as? String else {
            return
        }
        guard let autoRenewal =  attributes["auto-renewal"] as? Bool else {
            return
        }
        guard let primarySubscription =  attributes["primary-subscription"] as? Bool else {
            return
        }
        
        self.subscription = SubscriptionDetails.init(subscrption_id: suscription_id, type: type, dataBalance: dataBalance, expiryDate: expireDate, auto_renewal: autoRenewal, primary_subscription: primarySubscription)
        
    }
    
    // Get the product details
    func getProductDetails(product: [String:Any]) {
        var attributes : [String:Any] =  product["attributes"] as! [String : Any]
        guard let name = attributes["name"] as? String else {
            return
        }
        guard let price = attributes["price"] as? Int else {
            return
        }
        self.product = ProductDetails.init(product_Name: name, price: price)
    }
    
    @IBAction func actionLogin(_ sender: UIButton) {
        if (self.validateEmail(enterEmail: textField.text!)) {
            if (textField.text == self.user!.emailaddress) {
                let viewController = DetailedViewController.newViewcotroller()
                viewController.title_screen = user!.title
                viewController.subscription = self.subscription!
                viewController.product = self.product!
                self.navigationController?.pushViewController(viewController, animated: true)
            }else {
                self.showTextAlertMessage(title: "", message: "Entered emailid is not registered")

            }
        }else {
            self.showTextAlertMessage(title: "", message: "Please enter the valid email id")
        }
        
    }
    
    //Email validation
    func validateEmail(enterEmail:String) -> Bool{
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@",emailFormat)
        return emailPredicate.evaluate(with:enterEmail)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
}

