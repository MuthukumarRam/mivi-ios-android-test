//
//  UserDetails.swift
//  Mivi
//
//  Created by Muthukumar on 08/05/18.
//  Copyright © 2018 Muthukumar. All rights reserved.
//

import Foundation
struct UserInfo {
    var id: String?
    var type: String?
    var contantnumber: String?
    var dateofbirth : String?
    var emailaddress: String?
    var firstname: String?
    var lastname: String?
    var title: String?
    
    
    init(id: String?, type: String?, contantnumber: String?, dateofbirth: String?, emailaddress: String?, firstname: String?, lastname: String?, title: String) {
        self.id = id
        self.type = type
        self.contantnumber = contantnumber
        self.dateofbirth = dateofbirth
        self.emailaddress = emailaddress
        self.firstname = firstname
        self.lastname = lastname
        self.title = title
    }
    
}
