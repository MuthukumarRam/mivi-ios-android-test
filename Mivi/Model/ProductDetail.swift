//
//  ProductDetail.swift
//  Mivi
//
//  Created by Brajesh on 5/8/18.
//  Copyright © 2018 Muthukumar. All rights reserved.
//

import Foundation
struct ProductDetails {
    var product_Name: String?
    var price: Int
    init(product_Name: String?, price: Int) {
        self.product_Name = product_Name
        self.price = price
    }
}
