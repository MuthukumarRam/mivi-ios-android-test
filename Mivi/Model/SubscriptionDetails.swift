//
//  SubscriptionDetails.swift
//  Mivi
//
//  Created by Brajesh on 5/8/18.
//  Copyright © 2018 Muthukumar. All rights reserved.
//

import Foundation
struct SubscriptionDetails {
    var subscrption_id: String?
    var type: String?
    var dataBalance: Int?
    var expiryDate: String?
    var auto_renewal: Bool?
    var primary_subscription: Bool?
    
    
    init(subscrption_id: String?, type: String?, dataBalance: Int?, expiryDate: String?, auto_renewal: Bool?, primary_subscription: Bool?) {
        self.subscrption_id = subscrption_id
        self.type = type
        self.dataBalance = dataBalance
        self.expiryDate = expiryDate
        self.auto_renewal = auto_renewal
        self.primary_subscription = primary_subscription

}
}
